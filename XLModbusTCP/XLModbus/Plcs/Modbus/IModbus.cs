﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XLModbus.Plcs.Modbus
{
    public interface IModbus
    {
        bool SetRegister(int iaddress, object value);
        bool SetRegister<TValue>(int iaddress, List<object> value) where TValue : struct;
        bool SetBitRegister(int iaddress, int offset, bool value);
        bool SetBitRegister(int iaddress, ushort old_value, int offset, bool value);
        bool SetBitsRegister(int iaddress, List<bool> value);
        bool SetRegister(int iaddress, ushort[] value);
        List<bool> ReadBitsRegister(int iaddress, int count);
        bool ReadBitRegister(int iaddress, int offset = 0);
        TValue ReadRegister<TValue>(int iaddress) where TValue : struct;
        List<ushort> ReadRegister(int iaddress, int count);
        T ReadClass<T>(int iaddress) where T : class;

        ushort ReadInputRegister(int iaddress);
        List<ushort> ReadInputRegister(int iaddress, int count);
        bool ReadInputBitRegister(int iaddress, int offset = 0);
        List<bool> ReadInputBitsRegister(int iaddress, int count);

        bool SetCoil(int iaddress, bool value);
        bool SetCoil(int iaddress, bool[] value);
        List<bool> GetCoil(int iaddress, int count = 0);
        bool GetCoil(int iaddress);
        List<bool> GetDiscreteInput(int iaddress, int count = 0);
        bool GetDiscreteInput(int iaddress);

        bool Open();
        bool Close();
    }
}
